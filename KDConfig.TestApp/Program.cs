﻿using System;
using KDConfig.Providers;

namespace KDConfig.TestApp
{
  class Program
  {
    public class ConfigSubModel1
    {
      [ConfigValue("subvalue1")]
      public int Value1;
    }

    public class ConfigModel1
    {
      [ConfigValue("section.value1")]
      public int Value1;
      //
      [ConfigValue("section.value2")]
      public int Value2 { get; }
      //
      [ConfigValue("section.submodel")]
      public ConfigSubModel1 Submodel { get; }
      //
      // [ConfigValue("section.value_str")]
      // public string ValueStr { get; }
      //
      // [ConfigValue("section.opt", RequiredEnum.Optional)]
      // public string Opt { get; }

      // [ConfigValue("section.empty", RequiredEnum.Optional, EmptyHandling.AsMissing)]
      // public string EmptyStr { get; } = "Q";

      // [ConfigValue("section.emptyint", RequiredEnum.Required)]
      // public int? EmptyInt { get; } = 234;

      // [ConfigValue("section.opt2", RequiredEnum.Optional)]
      // public int Opt2 { get; } = 5;

      // [ConfigValue("section.invalid")]
      // public int Invalid
      // {
      //   get { return 0; }
      //   set { Console.WriteLine("test"); }
      // }
    }

    static void Main(string[] args)
    {
      var s = @"
section:
  value1: 1
  value2: 456
  value_str: asd
  empty:  s

  submodel:
    subvalue1a: 999
".Trim();

      try {

        var r = KDConfig.CreateFrom<ConfigModel1>(YamlConfigDataProvider.FromYamlString(s));
      }
      catch (ConfigException e) {
        Console.WriteLine(e);
      }
      //
      // // Console.WriteLine(r.EmptyStr is null);
      // // Console.WriteLine(r.EmptyStr);
      // Console.WriteLine(r.EmptyInt is null);
      // Console.WriteLine(r.EmptyInt);
    }
  }
}